<?php
// vim: ts=4 sw=4 noet tw=78 fo=croqn
/**
 * This class holds the settings for the TransIP API.
 * 
 * @package Transip
 * @class ApiSettings
 * @author TransIP (support@transip.nl)
 */
class Transip_ApiSettings
{
	const RE_VALIDATE = '/(-{5})BEGIN (RSA)?PRIVATE KEY\1(.*)\1END (RSA)?PRIVATE KEY\1/si';
	/**
	 * The mode in which the API operates, can be either:
	 *		readonly
	 *		readwrite
	 *
	 * In readonly mode, no modifying functions can be called.
	 * To make persistent changes, readwrite mode should be enabled.
	 */
	public static $mode = 'readwrite';

	 /**
	 * TransIP API endpoint to connect to.
	 *
	 * e.g.:
	 *
	 * 		'api.transip.nl'
	 * 		'api.transip.be'
	 * 		'api.transip.eu'
	 */
	public static $endpoint = 'api.transip.nl';

	/**
	 * Your login name on the TransIP website.
	 */
	protected static $_login = '';

	/**
	 * One of your private keys; these can be requested via your Controlpanel
	 */
	protected static $_privateKey = '';

	/**
	 * Path to private key
	 */
	protected static $_privateKeyPath = '';

	/**
	 * Flag indicating if the configuration is read from apisettings.ini
	 */
	protected static $_configRead = false;

	/**
	 * Flag indicating if the key has been validated and fixed.
	 */
	protected static $_keyFixed = false;

	/**
	 * Get the login name.
	 *
	 * @return string login
	 */
	public static function getLogin()
	{
		if( empty(self::$_login) )
			self::readConfig();

		return self::$_login;
	}

	/**
	 * Get the private key
	 *
	 * @return string privkey
	 */
	public static function getPrivateKey()
	{
		if( empty(self::$_privateKey) )
			self::readConfig();

		if( self::$_keyFixed !== true )
			self::$_privateKey = self::fixupPrivateKey(self::$_privateKey);

		return self::$_privateKey;
	}

	protected static function fixupPrivateKey($key)
	{
		$matches = array();
		if( !preg_match(self::RE_VALIDATE, $key, $matches) )
		{
			throw new InvalidArgumentException(
				'Invalid private key.'
			);
		}
		$key = $matches[3];
		$key = preg_replace('/\s*/s', '', $key);
		$key = chunk_split($key, 64, "\n");
		$key = "-----BEGIN PRIVATE KEY-----\n" . $key . "-----END PRIVATE KEY-----";
		self::$_keyFixed = true;
		return $key;
	}

	/**
	 * Invalidate the configuration. Useful for longlived applications that
	 * for instance implement a SIGHUP signal.
	 */
	public static function invalidateConfig()
	{
		self::$_configRead = false;
	}

	/**
	 * Reload the private key from file. Useful for longlived applications to
	 * be able to refresh the key without service interruption. Typically one
	 * would use a SIGUSR1 for this.
	 */
	public static function reloadPrivateKey()
	{
		self::$_privateKey = file_get_contents(self::$_privateKeyPath);
	}

	/**
	 * Read the configuration from apisettings.ini and set internal variables
	 * accordingly.
	 * Throws InvalidArgumentException for various error conditions.
	 *
	 * Can be extended to allow reading from database, LDAP or simply a
	 * different path.
	 */
	protected static function readConfig()
	{
		if( self::$_configRead === true )
			return;

		$path = realpath(self::pathJoin(dirname(__FILE__), '..', 'etc', 
			'apisettings.ini'));
		$settings = parse_ini_file($path);
		if( isset($settings['login']) )
			self::$_login = $settings['login'];
		else
			throw new InvalidArgumentException(
				'login unset in apisettings.ini'
			);
		if( isset($settings['keyfile']) )
		{
			if( is_readable($settings['keyfile']) )
			{
				self::$_privateKeyPath = realpath($settings['keyfile']);
				self::reloadPrivateKey();
			}
			else
			{
				throw new InvalidArgumentException(
					sprintf(
						'Unable to read private key file: %s',
						$settings['keyfile']
					)
				);
			}
		}
		else
		{
			throw new InvalidArgumentException(
				'keyfile unset in apisettings.ini'
			);
		}
		self::$_configRead = true;
	}

	/**
	 * Join path components OS independently.
	 */
	private static function pathJoin()
	{
		$argv = func_get_args();
		return implode(DIRECTORY_SEPARATOR, $argv);
	}
}
