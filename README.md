TransIP API Reference
---------------------

First and foremost, I'm a satisfied customer of TransIP and there stops my
affiliation.

This repo contains the reference implementation for their API and I'm simply
adding some enhancements/changes that I think are important or handy. Whether
these become part of the official API lies not with me, but with TransIP.

Use at your own risk.
